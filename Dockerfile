
# pulls a nodeJS image:version from docker hub
FROM node:15
# creates app directory in container
WORKDIR /app
# bundles app source code inside the Docker image
COPY . .
# gives ability to use npm commands (nodes modules)
RUN npm install
# defines what command will run my app
CMD node server.js

RUN apt-get update; apt-get install curl
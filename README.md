# Application d'exemple pour le travail pratique sur le Cloud

## Installation

```bash
git clone https://gitlab.com/BDWA-SAS-1/cloud/example-app
cd example-app
npm install # Installation des dépendances
```

## Lancement

```bash
npm startz
```

## Utilisation de Docker dans un pipeline GitLab CI

Pour utiliser Docker dans un pipeline, ajouter ces deux lignes en haut du fichier `.gitlab-ci.yml`:

```yaml
services:
  - docker:dind
```
